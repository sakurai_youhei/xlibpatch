.PHONY: all
all: clean build

.PHONY: build
build: clean
	gcc -shared -fPIC -o xlibpatch-xnextevent.so xlibpatch-xnextevent.c -ldl

.PHONY: clean
clean:
	rm -f xlibpatch-xnextevent.so

.PHONY: install
install:
	cp -f xlibpatch-xnextevent.so /usr/local/lib

