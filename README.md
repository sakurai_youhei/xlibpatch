# What's this

xlibpatch-xnextevent.so intercepts XNextEvent function to turn off ```XEvent.xany.send_event``` flag.

This works around the problem mentioned in xdotool's documentation - Many programs *reject* XEvent which xdotool generates to a specific window.

http://www.semicomplete.com/projects/xdotool/xdotool.xhtml

> If you are trying to send key input to a specific window, and it does not appear to be working, then it's likely your application is ignoring the events xdotool is generating. This is fairly common.

> Sending keystrokes to a specific window uses a different API than simply typing to the active window. If you specify 'xdotool type --window 12345 hello' xdotool will generate key events and send them directly to window 12345. However, X11 servers will set a special flag on all events generated in this way (see XEvent.xany.send_event in X11's manual). Many programs observe this flag and reject these events.

# Prerequisite

```
apt-get -yq install build-essential libx11-dev
```

# How to build & install

```
make build install
```

# How to use

```
Xvfb -nolisten tcp -noreset :0 -screen 0 1024x768x8 &
LD_PRELOAD=/usr/local/lib/xlibpatch-xnextevent.so DISPLAY=:0 xclock
LD_PRELOAD=/usr/local/lib/xlibpatch-xnextevent.so DISPLAY=:0 firefox
```

# TODO

- Change ```orig_XNextEvent``` to a static variable for better performance.