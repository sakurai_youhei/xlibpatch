#define _GNU_SOURCE 1

#include <dlfcn.h>
#include <X11/Xlib.h>

// http://www.semicomplete.com/blog/geekery/xsendevent-xdotool-and-ld_preload.html
// http://blog.dhampir.no/content/using-ld_preload-to-override-a-function

extern int XNextEvent(Display* display, XEvent* event_return){
	int r;
	int (*orig_XNextEvent)(Display*, XEvent*) = dlsym(RTLD_NEXT, "XNextEvent");
	r = orig_XNextEvent(display, event_return);
	event_return->xany.send_event = False;
	return r;
}

